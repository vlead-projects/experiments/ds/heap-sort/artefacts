#+TITLE: Common Styles for Heap Sort Interactive artefacts
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil
 
* Introduction
  This document builds the common styles which, can be used
  across all the html interactive artefacts.

* Styles
** CSS for wrapper
#+NAME: style-wrapper
#+BEGIN_SRC css
#wrapper {
    padding-top: 2%;
}

#+END_SRC
** Defining the text formatting
#+NAME: text-formatting
#+BEGIN_SRC css
body {
  font-family : "Helvetica Neue",Helvetica,Arial,sans-serif;
  /* text-align: center; */
}

.text-display-1 {
  font-family : "Helvetica Neue",Helvetica,Arial,sans-serif;
  font-size: large;
  font-weight: bold;
  text-align: center;
}

.text-display-2 {
  font-family : "Helvetica Neue",Helvetica,Arial,sans-serif;
  font-weight: bold;
  text-align: center;
}

.text-display-3 {
  font-family : "Helvetica Neue",Helvetica,Arial,sans-serif;
  text-align: center;
}

#+END_SRC

** Overriding Common CSS forms
#+NAME: form-elems
#+BEGIN_SRC css
.button-input:disabled {
  background-color: rgb(150, 160, 163) !important;
  cursor: not-allowed  !important;
}
.button-input {
  margin-top: 10px;
}

input[type="number"] {
  width: 250px;
}

#+END_SRC

** Style for the Legend Backgrounds
#+NAME: legend
#+BEGIN_SRC css
.legend .orange { background-color: #fb8415; }
.legend .red { background-color: #f80000; }
.legend .black { background-color: #000000; }
.legend .green { background-color: rgb(152, 203, 59); }
.legend .blue { background-color: rgb(23, 102, 150); }

#+END_SRC

** Style Animation Controller Section
#+NAME: animation-controller-section
#+BEGIN_SRC css
#generalAnimationControlSection {
  display: None;
}

#+END_SRC

* Tangle
#+BEGIN_SRC css :tangle style_main.css :eval no :noweb yes
<<text-formatting>>
<<form-elems>>
<<legend>>
<<animation-controller-section>>
#+END_SRC
