#+TITLE: Demonstration artefact for Extraction in Heap sort
#+AUTHOR: VLEAD
#+DATE:
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =Extract in Heapsort Demo=
  interactive artefact(HTML).

* Features of the artefact
  + Inserts a new value into the heap
  + Renders both the tree image and the array image
  + Lets the user see the Heapify swaps as animations

* Code
** Setting up the the HTML and fetching all CSS dependencies
Calling Bootstrap and Treant CSS, andjQuery
#+NAME: head-section-elems
#+BEGIN_SRC html
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width">
<title>Demo Extract</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
      crossorigin="anonymous">
<link href="../css/style_main.css" rel="stylesheet">
<link rel="stylesheet" href="../../static/lib/jquery-ui-1.8.11.custom.css">
<script src="../../static/lib/jquery-1.5.2.min.js"></script>
<script src="../../static/lib/jquery-ui-1.8.11.custom.min.js"></script>
<script src="../../static/animation/CustomEvents.js"> </script>
<script src="../../static/animation/UndoFunctions.js"> </script>
<script src="../../static/animation/AnimatedObject.js"> </script>
<script src="../../static/animation/AnimatedLabel.js"> </script>
<script src="../../static/animation/AnimatedCircle.js"> </script>
<script src="../../static/animation/AnimatedRectangle.js"> </script>
<script src="../../static/animation/AnimatedLinkedList.js"> </script>
<script src="../../static/animation/HighlightCircle.js"> </script>
<script src="../../static/animation/Line.js"> </script>
<script src="../../static/animation/ObjectManager.js"> </script>
<script src="../../static/animation/AnimationMain.js"> </script>
<script src="../../static/animation/Algorithm.js"> </script>
<script src="../js/demo_extract.js"> </script>
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
#+END_SRC

** Instruction Box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instructions-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li>Click <b>Populate</b> to fill the heap and wait
      for the build.</li>
      <li>Click <b>Extract</b> and observe how the
      Extract-Min process is performed.</li>
    </ul>
  </div>
</div>

#+END_SRC

** CANVAS for Drawing the Heap
Drawing canvas where all animation is done.  
Note: can be resized in code.
#+NAME: canvas
#+BEGIN_SRC html
<div id="mainContent" style="text-align: center;">
  <canvas id="canvas" width="825" height="400"></canvas>
</div>

#+END_SRC

** Observations are to see the message being displayed for every step
This is a box in which the comments gets displayed, based on
what is happening currently in the artefact.
#+NAME: observations
#+BEGIN_SRC html
<div class="comment-box" id="comment-box-bigger" align="center" style="margin: auto; height: 10vh;">
  <b>Observations</b>
  <p id="nextcomment"> </p>
</div>

#+END_SRC

** Buttons are used for functionality
This is a div wrapped with buttons used for
performing/demonstrating an artefact.
#+NAME: buttons
#+BEGIN_SRC html
<div id="buttons-wrapper" style="text-align: center;">
  <button class="button-input btn-color-green col-md-3" id="heap-extdemo-insert-btn">Populate</button>
  <button class="button-input btn-color-blue col-md-3 offset-md-1" id="heap-extdemo-extract-btn">Extract-Min</button>
</div>

#+END_SRC

** Animation controls for the experiment
#+NAME: animation-controller-btns
#+BEGIN_SRC html
<div id="generalAnimationControlSection">
  <table id="GeneralAnimationControls"> </table>
</div>

#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle demo_extract.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body onload="init();" class="VisualizationMainPage">
     <div class="container-fluid" style="padding-left: 0 !important;padding-right: 0 !important;">
        <<instructions-box>>
        <div id="wrapper" style="text-align=center;">
           <<canvas>>
           <<observations>>
           <<buttons>>
           <<animation-controller-btns>>
        </div>
     </div>
     <script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
     </body>
</html>
#+END_SRC

